//Add an event to the regiter form

let registerForm = document.querySelector("#registerUser")
console.log(registerForm)

//add an event listener submit 

registerForm.addEventListener('submit',(e) =>{
	e.preventDefault()

//add query selector in all input fields

let firstName = document.querySelector("#firstName").value;
let lastName = document.querySelector("#lastName").value;
let mobileNo = document.querySelector("#mobileNumber").value;
let email = document.querySelector("#userEmail").value;
let password1 = document.querySelector("#password1").value;
let password2 = document.querySelector("#password2").value;

//validation to enable submit button when all field are populated and both password match. and mobile number is = 11
	if ((password1 !== '' && password2 !== '') &&(password2 === password1) &&(mobileNo.length ===11)){
//check for duplicate email in db first
		fetch('http://localhost:3000/api/users/email-exists', {method: 'POST',
			header: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		//if no duplicateds found
		
if (data === false){
	fetch('http://localhost:3000/api/users/',{
				method: 'POST' ,
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNo
				})	
			})
			.then(res => res.json())
			.then(data =>{
				console.log(data)
				if(data === true) {
					//registration successful
					alert("Registered Successfully")
					//redirect login
					window.location.replace("./login.html");
				}else{
					alert("Something went wrong");
				}
			})
			.catch((error)=>{
				console.error('Error, error')
			});
			}
		    })

	}else {
		alert("Please check the infomation provided")
	}

})
